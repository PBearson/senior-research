\contentsline {section}{\numberline {1}Introduction}{7}
\contentsline {section}{\numberline {2}Background}{10}
\contentsline {subsection}{\numberline {2.1}Current Shortcomings}{11}
\contentsline {subsection}{\numberline {2.2}Containerization}{13}
\contentsline {subsection}{\numberline {2.3}Certification}{17}
\contentsline {subsection}{\numberline {2.4}Additional Studies}{19}
\contentsline {section}{\numberline {3}Theory of Model}{19}
\contentsline {subsection}{\numberline {3.1}Certificates}{22}
\contentsline {subsection}{\numberline {3.2}Containers}{23}
\contentsline {subsection}{\numberline {3.3}Application Development}{24}
\contentsline {subsection}{\numberline {3.4}Maintainability}{25}
\contentsline {section}{\numberline {4}Methodology}{25}
\contentsline {subsection}{\numberline {4.1}List of Equipment and Services}{26}
\contentsline {subsection}{\numberline {4.2}Configuration \& Topology}{28}
\contentsline {subsection}{\numberline {4.3}Watchdogs}{30}
\contentsline {subsection}{\numberline {4.4}Database Design}{32}
\contentsline {subsection}{\numberline {4.5}Initialization and Post-Initialization}{34}
\contentsline {subsection}{\numberline {4.6}Application Development}{37}
\contentsline {section}{\numberline {5}Analysis}{38}
\contentsline {subsection}{\numberline {5.1}Future Work}{39}
\contentsline {subsection}{\numberline {5.2}Insufficient Authentication}{40}
\contentsline {subsection}{\numberline {5.3}Lack of Transport Encryption}{41}
\contentsline {subsection}{\numberline {5.4}Insecure Network Services}{41}
\contentsline {subsection}{\numberline {5.5}Poor Physical Security}{42}
\contentsline {section}{\numberline {6}Conclusion}{43}
\contentsline {section}{\numberline {7}Appendix}{47}
\contentsline {subsection}{\numberline {7.1}Appendix A: Configuration File Template}{47}
\contentsline {subsection}{\numberline {7.2}Appendix B: Pseudocode}{48}
