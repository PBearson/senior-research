import random, time

co2 = 250
maxCo2 = 5000
minCo2 = 250

while True:
    time.sleep(30)
    dev = random.randint(0, 50)
    maxDiff = maxCo2 - co2
    minDiff = co2 - minCo2

    direct = random.randint(0, maxDiff + minDiff)
    
    if direct < maxDiff :
        co2 = co2 + dev
        if co2 > maxCo2:
            co2 = maxCo2
    else:
        co2 = co2 - dev
        if co2 < minCo2:
            co2 = minCo2

    print(co2)
    o = open("data", "a")
    o.write(str(co2) + "\n")
    o.close()    
