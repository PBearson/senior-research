<?php header("Content-type: text/javascript");?>
<?php include "../../model/db-connect.php"?>
var ctx = document.getElementById('humChart');
var chart = new Chart(ctx,
{
	type: 'line',
	data:
	{
		labels: [<?php foreach ($humDates as $dates)
		{
			echo "'" . date("H:i:s", strtotime($dates[0])) . "'" . ", ";
		}?>],
		datasets: 
		[{
			borderColor: 'rgb(50, 255, 0)',
			data: [<?php foreach ($humData as $h)
			{
				echo $h[0] . ", ";
		}?>],
			fill: false,
		}]
	},
	options: 
	{
		animation: false,
		legend:
		{
			display: false
		},
		tooltips:
		{
			enabled: false
		},
		scales:
		{
			xAxes:
			[{
				ticks:
				{
					fontSize: 14,
					fontStyle: 'bold',
					maxRotation: 45,
					minRotation: 45
				}
			}]
		}
	}
});

chart.render();
