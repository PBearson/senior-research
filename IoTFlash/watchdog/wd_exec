#!/usr/bin/env python

'''
The watchdog execute script is placed on every master, along with a configuration file containing a "DATA" argument.
When run, the master looks at all other nodes and pulls the data that contains a matching "DATA" argument. Then the
data is deleted from the other nodes and kept on the master node.

It should be made clear that multiple DATA arguments can be specified in a master, but not in a regular node (since 
each regular node is designed a run a single application). In the former case, each data collection is performed sequentially,
since the server, meanwhile, will be looking at a single 'data' file on the master node, ready to pull from it once the master
has gone through all the nodes.
'''

import os, json, re
from sys import argv
from fabric.api import *

dataFile = ""
srcDir = ""
configFile = ""
masterKey = ""
localData = []
 
# Parse the remote device's config file for the DATA tag.
# If a match is found, pull data to the local device and delete
# data from remote device. Assume we are already SSH'd into
# the target device.
def checkRemoteDevice(device):
	global dataFile, configFile, localData, srcDir
	
	# Get the DATA tag
	f = run('cat %s | grep DATA' % configFile)
	
	for line in f.splitlines():
		if not line:
			break
		if re.match(r'^\s*$', line):
			continue

		lineArgs = line.split()

		remoteData = lineArgs[1]

	if remoteData not in localData:
		return

	# Get the data
	local('touch %s/data-output/%s/%s' % (srcDir, remoteData, device))
	data = run('cat %s' % dataFile)

	for line in data.splitlines():
		if not line:
			break
		if re.match(r'^\s*$', line):
			continue

		line = line.replace('\x1b(B\x1b[m', '')

		local('echo %s | sudo tee -a %s/data-output/%s/%s' % (line, srcDir, remoteData, device))

	# Remove old data
	run('sudo rm %s && touch %s' % (dataFile, dataFile))

# Same as remote, except we are looking at the local device
def checkLocalDevice(device):
	global dataFile, configFile, localData, srcDir
	
	# Get the DATA tag
	f = open(configFile, "r")
	while True:
		line = f.readline()
		if not line:
			break
		if re.match(r'^\s*$', line):
			continue
	
		lineArgs = line.split()

		if(lineArgs[0] == "*DATA*"):
			remoteData = lineArgs[1]

	if remoteData not in localData:
		return
	
	# Get the data
	local('touch %s/data-output/%s/%s' % (srcDir, remoteData, device))
	f = open(dataFile, "r")
	while True:
		line = f.readline()
		if not line:
			break
		if re.match(r'^\s*$', line):
			continue

		lineArgs = line.split()
		
		local('echo %s | sudo tee -a %s/data-output/%s/%s' % (lineArgs[0], srcDir, remoteData, device))

	# Remove old data
	local('sudo rm %s && touch %s' % (dataFile, dataFile))
		
### Start here ###

# Expected arguments:
# 1: A dict of all devices, and their associated usernames
# 2: The source directory for all files except the private key
# 3: The location of the private key on the master
# 4: The location of the data file.
# 5: The location of the config file.

devices = json.loads(argv[1])
srcDir = argv[2]
masterKey = argv[3]
dataFile = '%s/%s' % (srcDir, argv[4])
configFile = '%s/%s' % (srcDir, argv[5])

# Create output file
try:
	local('mkdir %s/data-output' % srcDir)
except:
	pass

# Create exec flag (for server to recognize that data pull
# is in-progress
try:
	local('touch %s/exec-flag' % srcDir)
except:
	pass

# Get DATA tags and create subdirectories for them
f = open(configFile, "r")
while(True):
	line = f.readline()
	if not line:
		break
	if re.match(r'^\s*$', line):
		continue

	lineArgs = line.split()

	if lineArgs[0] == "DATA" or lineArgs[0] == "*DATA*":
		localData.append(lineArgs[1])
		try:
			local('mkdir %s/data-output/%s' % (srcDir, lineArgs[1]))
		except:
			pass

# Pull data from other nodes, then restart containers
for d in devices:
	selfHostName = local('hostname', capture=True)

	if(selfHostName == d):
		checkLocalDevice(d)
		containers = local("docker ps --quiet", capture=True).split()
		for c in containers:
			local('docker restart %s' % c)
	else:
		with settings(host_string = "%s.local" % d, user = devices[d], key_filename = masterKey):
			checkRemoteDevice(d)
			containers = run("docker ps --quiet").split()
			for c in containers:
				c = c.replace('\x1b(B\x1b[m', '')
				run("docker restart %s" % c)


# Remove flag
try:
	local('sudo rm %s/exec-flag' % srcDir)
except:
	pass
