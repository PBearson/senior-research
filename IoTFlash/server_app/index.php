<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="view/css/materialize.min.css" media="screen,projection">
	<meta name = "viewport" content="width=device-width, initial-scale=1.0"/>
	<style>
	body 
	{
		background-color: #BBFFBB;
	}
	</style>
<head>

<body>
	<center><h2>Random Data Showcase</h2></center>
	<center><h4>Temperature Values:</h4></center>
	<canvas id="tempChart" height="25%" width="75%"></canvas>
	
	<center><h4>Humidity Values:</h4></center>
	<canvas id = "humChart" height="25%" width="75%"></canvas>

	<center><h4>CO2 Values:</h4></center>
	<canvas id = "co2Chart" height="25%" width="75%"> </canvas>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<script type="text/javascript" src="view/js/tempChart.php"></script>
	<script type="text/javascript" src="view/js/humChart.php"></script>
	<script type="text/javascript" src="view/js/co2Chart.php"></script>
	<script type="text/javascript" src="view/js/materialize.min.js"</script>
</body>
</html>
