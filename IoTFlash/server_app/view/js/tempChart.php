<?php header("Content-type: text/javascript");?>
<?php include "../../model/db-connect.php"?>

var ctx = document.getElementById('tempChart');
var chart = new Chart(ctx,
{
	type: 'line',
	data:
	{
		labels: [<?php foreach ($tempDates as $dates)
		{
			echo "'" . date("H:i:s", strtotime($dates[0])) . "'" . ", ";
		}?>],
		datasets: 
		[{
			borderColor: 'rgb(255, 0, 50)',
			data: [<?php foreach ($tempData as $t)
			{
				echo $t[0] . ", ";
		}?>],
			fill: false,
		}]
	},
	options: 
	{
		animation: false,
		legend:
		{
			display: false
		},
		tooltips:
		{
			enabled: false
		},
		scales:
		{
			xAxes:
			[{
				ticks:
				{
					fontSize: 14,
					fontStyle: 'bold',
					maxRotation: 45,
					minRotation: 45
				}
			}]
		}
	}
});

chart.render();
